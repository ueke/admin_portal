package com.coronationam.AdminPortal.modules.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "userlogin")
public class CustomerLoginRecord {
	
    @Id
    @GeneratedValue
    private Long id;
    
    private String userid;
    private String name;
    private String email;
    private String phone;
    
    @Column(name = "date_loggedin", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss") 
    private Date dateloggedin;
    
    private String device_type;
    
    
    
	public String getDevice_type() {
		return device_type;
	}
	public void setDevice_type(String device_type) {
		this.device_type = device_type;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    
    
   
	
	public Date getDateloggedin() {
		return dateloggedin;
	}
	public void setDateloggedin(Date dateloggedin) {
		this.dateloggedin = dateloggedin;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	/*
	public String getDate_loggedin() {
		return date_loggedin;
	}
	public void setDate_loggedin(String date_loggedin) {
		this.date_loggedin = date_loggedin;
	}
	*/
    
    

}
