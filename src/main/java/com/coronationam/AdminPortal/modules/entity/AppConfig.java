package com.coronationam.AdminPortal.modules.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "app_config")
public class AppConfig {

    @Id
    @GeneratedValue
    private Long id;
    private String appbase_url;
    private String infoware_baseurl;
    private String bank_acct_number;
    private String bank_code;
    private String credit_acct_master; 
    private String debit_acct_master;
    private String debit_acct_sub;  
    
    private String minimum_investment;
    private String paystack_callback_url;
    
    
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAppbase_url() {
		return appbase_url;
	}
	public void setAppbase_url(String appbase_url) {
		this.appbase_url = appbase_url;
	}
	public String getInfoware_baseurl() {
		return infoware_baseurl;
	}
	public void setInfoware_baseurl(String infoware_baseurl) {
		this.infoware_baseurl = infoware_baseurl;
	}
	public String getBank_acct_number() {
		return bank_acct_number;
	}
	public void setBank_acct_number(String bank_acct_number) {
		this.bank_acct_number = bank_acct_number;
	}
	public String getBank_code() {
		return bank_code;
	}
	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}
	public String getCredit_acct_master() {
		return credit_acct_master;
	}
	public void setCredit_acct_master(String credit_acct_master) {
		this.credit_acct_master = credit_acct_master;
	}
	public String getDebit_acct_master() {
		return debit_acct_master;
	}
	public void setDebit_acct_master(String debit_acct_master) {
		this.debit_acct_master = debit_acct_master;
	}
	public String getDebit_acct_sub() {
		return debit_acct_sub;
	}
	public void setDebit_acct_sub(String debit_acct_sub) {
		this.debit_acct_sub = debit_acct_sub;
	}
	public String getMinimum_investment() {
		return minimum_investment;
	}
	public void setMinimum_investment(String minimum_investment) {
		this.minimum_investment = minimum_investment;
	}
	public String getPaystack_callback_url() {
		return paystack_callback_url;
	}
	public void setPaystack_callback_url(String paystack_callback_url) {
		this.paystack_callback_url = paystack_callback_url;
	}  
    
    
    
}