package com.coronationam.AdminPortal.modules.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "audit_table")
public class ActivityLog {

    @Id
    @GeneratedValue
    private int id;
    private String loggedin_user;
    private String loggedin_date;
    private String phone;
    private String email;
    private String action_performed;
    
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoggedin_user() {
		return loggedin_user;
	}
	public void setLoggedin_user(String loggedin_user) {
		this.loggedin_user = loggedin_user;
	}
	public String getLoggedin_date() {
		return loggedin_date;
	}
	public void setLoggedin_date(String loggedin_date) {
		this.loggedin_date = loggedin_date;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAction_performed() {
		return action_performed;
	}
	public void setAction_performed(String action_performed) {
		this.action_performed = action_performed;
	}
    
       
    
}