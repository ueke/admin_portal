package com.coronationam.AdminPortal.modules.entity;

import javax.persistence.*;
import java.util.Set;


// This Table is for the mobile App users


@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue
    private int userid;
    private String firstname;
    private String lastname;
    private String othername;
    private int acct_status; //1-Active, 0-Inactive
    private String infoware_id;
    private String acct_bal;
    private String password;
    private String phone;
    
    // added
    private String date_created;

    @Column(name = "name", unique = true)
    private String email;

    @ManyToMany
    private Set<Roles> roles;


    public Set<Roles> getRoles() {
        return roles;
    }
    
  

    public String getDate_created() {
		return date_created;
	}



	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}



	public void setRoles(Set<Roles> roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getOthername() {
        return othername;
    }

    public void setOthername(String othername) {
        this.othername = othername;
    }

    public int getAcct_status() {
        return acct_status;
    }

    public void setAcct_status(int acct_status) {
        this.acct_status = acct_status;
    }

    public String getInfoware_id() {
        return infoware_id;
    }

    public void setInfoware_id(String infoware_id) {
        this.infoware_id = infoware_id;
    }

    public String getAcct_bal() {
        return acct_bal;
    }

    public void setAcct_bal(String acct_bal) {
        this.acct_bal = acct_bal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
