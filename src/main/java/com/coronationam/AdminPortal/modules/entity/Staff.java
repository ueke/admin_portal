package com.coronationam.AdminPortal.modules.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "staff")
public class Staff {

    @Id
    @GeneratedValue
    private Long id;
    private String role;
    private String name;
    private String email;
    private String phone;
    private String dept;
    private String passwordChangeRequired;  // 1-true, 0-false
    private String password;
    private String staffIsActive;   // 1-true, 0-false
    
    
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getPasswordChangeRequired() {
		return passwordChangeRequired;
	}
	public void setPasswordChangeRequired(String passwordChangeRequired) {
		this.passwordChangeRequired = passwordChangeRequired;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getStaffIsActive() {
		return staffIsActive;
	}
	public void setStaffIsActive(String staffIsActive) {
		this.staffIsActive = staffIsActive;
	}

    
    
}