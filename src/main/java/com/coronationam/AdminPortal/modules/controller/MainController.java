package com.coronationam.AdminPortal.modules.controller;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jasypt.util.text.AES256TextEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.coronationam.AdminPortal.modules.dao.ActivityLogRepository;
import com.coronationam.AdminPortal.modules.dao.AppConfigRepository;
import com.coronationam.AdminPortal.modules.dao.CustomerLoginRecordRepository;
import com.coronationam.AdminPortal.modules.dao.RedemptionRepository;
import com.coronationam.AdminPortal.modules.dao.StaffRepository;
import com.coronationam.AdminPortal.modules.dao.SupportRepository;
import com.coronationam.AdminPortal.modules.dao.TransactionLogRepository;

import com.coronationam.AdminPortal.modules.dao.UserRepository;

import com.coronationam.AdminPortal.modules.entity.ActivityLog;
import com.coronationam.AdminPortal.modules.entity.AppConfig;
import com.coronationam.AdminPortal.modules.entity.Redemption;
import com.coronationam.AdminPortal.modules.entity.Staff;
import com.coronationam.AdminPortal.modules.entity.Support;
import com.coronationam.AdminPortal.modules.entity.TransactionLog;
import com.coronationam.AdminPortal.modules.entity.User;
import com.coronationam.AdminPortal.modules.model.AppConfigModel;
import com.coronationam.AdminPortal.modules.model.AuditModel;
import com.coronationam.AdminPortal.modules.model.ChangePasswordModel;
import com.coronationam.AdminPortal.modules.model.ChartModel;
import com.coronationam.AdminPortal.modules.model.CustomerModel;
import com.coronationam.AdminPortal.modules.model.RedemptionModel;
import com.coronationam.AdminPortal.modules.model.Response;
import com.coronationam.AdminPortal.modules.model.StaffModel;
import com.coronationam.AdminPortal.modules.model.SupportModel;
import com.coronationam.AdminPortal.modules.model.TransactionModel;
import com.coronationam.AdminPortal.modules.model.loginmodel;
import com.coronationam.AdminPortal.modules.service.DataService;
import com.coronationam.AdminPortal.modules.entity.CustomerLoginRecord;

/* 
 
 
{
	"status" : success-"00", failed - "01" , Exception - "11"
	"message": "successful",
	"data": "Object"
	
}
  
  */

@CrossOrigin(origins = "http://localhost:50035" , maxAge = 3600)
@RestController
public class MainController {
	
	Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@Autowired
    private StaffRepository staffRepository;
	
	
	@Autowired
    private AppConfigRepository appConfigRepository;
	
	@Autowired
    private TransactionLogRepository transactionLogRepository;
	
	@Autowired
    private CustomerLoginRecordRepository customerLoginRecordRepository;
	
	
	
	
	
	@Autowired
    private UserRepository userRepository;
		
	@Autowired
    private ActivityLogRepository activityLogRepository;
	
	@Autowired
    private RedemptionRepository redemptionRepository;
	
	@Autowired
    private SupportRepository supportRepository;	
	
	@Autowired
    private DataService service;
	
	//Add staff
    @RequestMapping(value = "/createStaffAccount", method = RequestMethod.POST)
    public Object createStaffAccount(@RequestBody StaffModel req) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

            Staff staffDetail = staffRepository.findUserByEmail(req.getEmail());

            if (staffDetail != null) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("Email already exist");
            	
            } else {
            	Staff model = new Staff();
                model.setEmail(req.getEmail());
                model.setDept(req.getDept());
                model.setName(req.getName());
                model.setPhone(req.getPhone());
                model.setRole(req.getRole());
                model.setPasswordChangeRequired("1"); // 1-true, 0-false
                model.setPassword("XXetbaf@*vs");
                
                String isActive;
               	if(req.getStaffisactive() == null) {
                		isActive = "1";
                	}else {
                		isActive = req.getStaffisactive();
                	}
               	
               	
                	model.setStaffIsActive(isActive);  // staff account is activate
              
                
                staffRepository.save(model);
                response.setStatus("00");
                response.setData(null);
                response.setMessage("Account created Successfully");
                
                
                Staff staff = staffRepository.findUserByEmail(req.getLogging_user_email());
                
                CreateActivityLog(staff.getName(),staff.getPhone(),staff.getEmail(),
						 "Added new staff: "+req.getName());
                
                

                try {
                
                service.sendEmailVisitor(req.getEmail(),"Asset Management","Your account was created successfully. Below are your account details"+"\n"+" Customer ID: "+req.getEmail()+"\n"
                        +"Default password: XXetbaf@*vs");

                } catch (Exception ex) {
                    
                	response.setStatus("01");
                	response.setData(ex.getMessage());
                	response.setMessage("Your account creation might have been successful, please contact I.T for help"); // change this to - An error occur, please try again
                	
                	logger.info("Account creation failed, when sending mail: "+ex.getMessage());
                }

            }
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("Account creation failed"); // change this to - An error occur, please try again
        	
        	logger.info("Account creation failed, when sending mail: "+ex.getMessage());
      
        }
        return response;
       
    }
	

	//Update staff Account Detail
    @RequestMapping(value = "/updateStaffAccount", method = RequestMethod.POST)
    public Object updateStaffAccount(@RequestBody StaffModel req) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

            Optional<Staff> staffDetail = staffRepository.findById(Long.parseLong(req.getId()));

            if (!staffDetail.isPresent()) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("This user does not exist");
            	
            } else {
            	
            String isActive;
           	if(req.getStaffisactive() == null) {
            		isActive = "1";
            	}else {
            		isActive = req.getStaffisactive();
            	}
            	               
            	staffRepository.updateStaffRecord(req.getRole(), req.getEmail(), req.getPhone(), req.getDept(),isActive,Long.parseLong(req.getId()));
            	
                response.setStatus("00");
                response.setData(null);
                response.setMessage("Account updated Successfully");
                
                Staff staff = staffRepository.findUserByEmail(req.getLogging_user_email());
                
                if(staff!=null) {
                CreateActivityLog(staff.getName(),staff.getPhone(),staff.getEmail(),
						 "Updated staff record: "+req.getName());
                }

            }
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("Account update failed"); // change this to - An error occur, please try again
        	
        	logger.info("Account update failed: "+ex.getMessage());
      
        }
        return response;
       
    }
	

	//Login
    @CrossOrigin(origins = "http://localhost:50035" , maxAge = 3600)
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Object login(@RequestBody loginmodel req) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		Staff staffDetail = staffRepository.findUserByEmail(req.getEmail());

    	//	String inputedPass = encryptPassword(req.getPassword());
    //		String PasswdInDB = decryptPassword(staffDetail.getPassword());
    		
            if (staffDetail == null) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("User does not exist");
            	
     } else if((req.getPassword()).toLowerCase().equals(("XXetbaf@*vs").toLowerCase()) && staffDetail.getPasswordChangeRequired().equals("0")){
            		
    	response.setStatus("01");
    	response.setData(null);
    	response.setMessage("Incorrect password");
            		
     }
    else if((req.getPassword()).toLowerCase().equals(("XXetbaf@*vs").toLowerCase()) && staffDetail.getPasswordChangeRequired().equals("1")){
		
    		response.setStatus("00");
    		response.setData(staffDetail);
    		response.setMessage("Change Default password");
		
    }
    else if((req.getPassword()).equals(decryptPassword(staffDetail.getPassword()))){
		
        response.setStatus("00");
        response.setData(staffDetail);
        response.setMessage("success");
        
    
        
        
        CreateActivityLog(staffDetail.getName(),staffDetail.getPhone(),staffDetail.getEmail(),
						 "Logged in and view");
        
        
        
   }else {
	   
	   response.setStatus("01");
   	response.setData(null);
   	response.setMessage("Incorrect Username and password");
	   
   	}
            	
   } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("An error occur"); // change this to - An error occur, please try again 	
        	logger.info("login failed : "+ex.getMessage());
      
        }
    	
        return response;
       
    }
	

    //forgot password method
    @RequestMapping(value = "/forgotPassword/{email}", method = RequestMethod.POST)
    public Object forgotPassword(@PathVariable("email") String email) {

 	Response<Object> response = new Response<Object>();
    	
    	try {
    

    		Staff staffDetail = staffRepository.findUserByEmail(email);

            if (staffDetail == null) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("User does not exist");
            	
            } else {
            	
            	staffRepository.updatePasswordChangeRequired("1", email,"XXetbaf@*vs");
            	 response.setStatus("00");
                 response.setData(staffDetail);
                 response.setMessage("Your password reset was successful, check your mail for the reset password");
                 
                 
                 try {
                     
                     service.sendEmailVisitor(email,"Asset Management","Your password reset was successful. Login with the below account details"+"\n"+" username: "+email+"\n"
                             +"Default password: XXetbaf@*vs");

                    
                     } catch (Exception ex) {
                         
                     	response.setStatus("01");
                     	response.setData(ex.getMessage());
                     	response.setMessage("Your password reset was successful, check your mail for the reset password, please contact I.T for help, if you encounter any issues"); // change this to - An error occur, please try again
                     //	return response;
                     	logger.info("password set failed, when sending mail: "+ex.getMessage());
                     }
                 
                 
            }
        
    	} catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("An error occur"); // change this to - An error occur, please try again 	
        	logger.info("password reset failed : "+ex.getMessage());
      
        }

    	
    	
    	return response;
    }
    


	//Change password
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public Object changePassword(@RequestBody ChangePasswordModel req) {
    	
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		Staff staffDetail = staffRepository.findUserByEmail(req.getEmail());

            if (staffDetail==null) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("This user does not exist");
            	
            }
            
            else if(req.getCurrent_password().equals("XXetbaf@*vs") && staffDetail.getPasswordChangeRequired().equals("1")) {
            	
            	  staffRepository.updatePassword(encryptPassword(req.getNew_password()), req.getEmail(), "0");
                  response.setStatus("00");
                  response.setData(null);
                  response.setMessage("Account updated Successfully");
            }
            
            
            else if(!((req.getCurrent_password()).equals(decryptPassword(staffDetail.getPassword())))) {
            	
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("incorrect password");
            }
            
            else {
                staffRepository.updatePassword(encryptPassword(req.getNew_password()), req.getEmail(),"0");
                response.setStatus("00");
                response.setData(null);
                response.setMessage("Account updated Successfully");

            }
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("Account update failed"); // change this to - An error occur, please try again
        	
        	logger.info("Account update failed: "+ex.getMessage());
      
        }
        return response;
       
    }
	


    @GetMapping("/validFormOfID")
    public Object validFormOfID() {
        List<String> list = new ArrayList<>();
        list.add("Driver License");
        list.add("International Passport");
        list.add("National Identity Card");
        list.add("Voters Card");
        return list;
    }
    
    

    @GetMapping("/getTransaction")
    public Object getTransaction() {
    	
   Response<Object> response = new Response<Object>();
    	
    	try {
    		
    		List<TransactionLog> list= transactionLogRepository.findAll();
    		
    		response.setStatus("00");
        	response.setData(list);
        	response.setMessage("success");
    		
    	}catch(Exception ex) {
    		
    		response.setStatus("01");
        	response.setData(ex.getMessage());
        	response.setMessage("failed");
    		
    	}
        
    	return response;
    	
    }
    
    
    @GetMapping("/getTransaction/{start_date}/{end_date}")
    public Object getTransactionWithDateDeifference(@PathVariable("start_date") String start_date
    												,@PathVariable("end_date") String end_date) {
    	
   Response<Object> response = new Response<Object>();
    	
    	try {
    		Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(start_date); 
    		Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(end_date); 
    		
    		List<TransactionLog> list= transactionLogRepository.findByTransdateBetween(startDate,endDate);
    		
    		response.setStatus("00");
        	response.setData(list);
        	response.setMessage("success");
    		
    	}catch(Exception ex) {
    		
    		response.setStatus("01");
        	response.setData(ex.getMessage());
        	response.setMessage("failed");
    		
    	}
        
    	return response;
    	
    }
    
    
    
    @GetMapping("/getAllStaff")
    public Object getAllStaff() {
        
    	Response<Object> response = new Response<Object>(); 
    	
	try {
    		
    		List<Staff> list= staffRepository.findAll(); 
    		
    		response.setStatus("00");
        	response.setData(list);
        	response.setMessage("success");
    		
    	}catch(Exception ex) {
    		
    		response.setStatus("01");
        	response.setData(ex.getMessage());
        	response.setMessage("failed");
    		
    	}
        
    	return response;
   
    }
    
    
    @GetMapping("/getAllLoginAppUser")
    public Object getAllLoginAppUser() {
        
    	Response<Object> response = new Response<Object>(); 
    	
	try {
    		
    		List<CustomerLoginRecord> list= customerLoginRecordRepository.findAll(); 
    		
    		response.setStatus("00");
        	response.setData(list);
        	response.setMessage("success");
    		
    	}catch(Exception ex) {
    		
    		response.setStatus("01");
        	response.setData(ex.getMessage());
        	response.setMessage("failed");
    		
    	}
        
    	return response;
   
    }

    
    
    
  //Update staff Account Detail
    @GetMapping(value = "/getStaffWithID/{id}")
    public Object getStaffWithID(@PathVariable("id") String id) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		Optional<Staff> staffDetail = staffRepository.findById(Long.parseLong(id));

    		if(staffDetail.isPresent()) {
            	response.setStatus("00");
            	response.setData(staffDetail.get());
            	response.setMessage("This user does not exist");
    		}else {
    			response.setStatus("01");
            	response.setData(null);
            	response.setMessage("This user does not exist");
    		}
         
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("This user does not exist"); // change this to - An error occur, please try again
        	
        	logger.info("Account update failed: "+ex.getMessage());
      
        }
        return response;
       
    }
    
    

    @GetMapping("/getCustomer")
    public Object getCustomer() {
        
    	Response<Object> response = new Response<Object>(); 
    	
	try {
    		
    		List<User> list= userRepository.findAll(); 
    		
    		response.setStatus("00");
        	response.setData(list);
        	response.setMessage("success");
    		
    	}catch(Exception ex) {
    		
    		response.setStatus("01");
        	response.setData(ex.getMessage());
        	response.setMessage("failed");
    		
    	}
        
    	return response;
   
    }
    
    
    @GetMapping("/getActivityLog")
    public Object getActivityLog() {
        
    	Response<Object> response = new Response<Object>(); 
    	
	try {
    		
    		List<ActivityLog> list= activityLogRepository.findAll(); 
    		
    		response.setStatus("00");
        	response.setData(list);
        	response.setMessage("success");
    		
    	}catch(Exception ex) {
    		
    		response.setStatus("01");
        	response.setData(ex.getMessage());
        	response.setMessage("failed");
    		
    	}
        
    	return response;
   
    }
    

    

    @GetMapping("/getRedemptionRequest")
    public Object getRedemptionRequest() {
    	
    	Response<Object> response = new Response<Object>(); 
    	
    	try {
        		
        		List<Redemption> list= redemptionRepository.findAll(); 
        		
        		response.setStatus("00");
            	response.setData(list);
            	response.setMessage("success");
        		
        	}catch(Exception ex) {
        		
        		response.setStatus("01");
            	response.setData(ex.getMessage());
            	response.setMessage("failed");
        		
        	}
            
        	return response;
       
        }
    
    
    
    
    @GetMapping("/getRedemptionRequest/{id}")
    public Object getRedemptionRequestWithID(@PathVariable("id") String id)  {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		Optional<Redemption> redemption = redemptionRepository.findById(Long.parseLong(id));

    		if(redemption.isPresent()) {
            	response.setStatus("00");
            	response.setData(redemption.get());
            	response.setMessage("This request  exist");
    		}else {
    			response.setStatus("01");
            	response.setData(null);
            	response.setMessage("This request does not exist");
    		}
         
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("This request does not exist"); // change this to - An error occur, please try again
        	
        	logger.info("Account update failed: "+ex.getMessage());
      
        }
        return response;
       
    }
   
    
    @GetMapping(value = "/getSupportRequest/{id}")
    public Object getSupportRequest(@PathVariable("id") String id)  {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		Optional<Support> support = supportRepository.findById(Long.parseLong(id));

    		if(support.isPresent()) {
            	response.setStatus("00");
            	response.setData(support.get());
            	response.setMessage("This request  exist");
    		}else {
    			response.setStatus("01");
            	response.setData(null);
            	response.setMessage("This request does not exist");
    		}
         
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("This request does not exist"); // change this to - An error occur, please try again
        	
        	logger.info("Account update failed: "+ex.getMessage());
      
        }
        return response;
       
    }
    
    
    
  //Update staff Account Detail
    @RequestMapping(value = "/updateRedemptionRequest/{id}/{user_email}", method = RequestMethod.POST)
    public Object updateRedemptionRequest(@PathVariable("id") String id,@PathVariable("user_email") String user_email) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		redemptionRepository.updateRedemptionReqeust("completed",Long.parseLong(id));

            	response.setStatus("00");
            	response.setData(null);
            	response.setMessage("update successful");
            	
            	   
                Staff staff = staffRepository.findUserByEmail(user_email);
                
                if(staff!=null) {
                CreateActivityLog(staff.getName(),staff.getPhone(),staff.getEmail(),
						 "Complete redemption request");
                }
         
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("Account update failed"); // change this to - An error occur, please try again
        	
        	logger.info("Account update failed: "+ex.getMessage());
      
        }
        return response;
       
    }
    
    
    

    @GetMapping("/getSupportRequest")
    public Object getSupportRequest() {
    	
    	Response<Object> response = new Response<Object>(); 
    	
    	try {
        		
        		List<Support> list= supportRepository.findAll(); 
        		
        		response.setStatus("00");
            	response.setData(list);
            	response.setMessage("success");
        		
        	}catch(Exception ex) {
        		
        		response.setStatus("01");
            	response.setData(ex.getMessage());
            	response.setMessage("failed");
        		
        	}
            
        	return response;
       
        }
    
    
  //Update staff Account Detail
    @RequestMapping(value = "/updateSupportRequest/{id}/{user_email}", method = RequestMethod.POST)
    public Object updateSupportRequest(@PathVariable("id") String id, @PathVariable("user_email") String user_email) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		supportRepository.updateSupportReqeust("Completed",Long.parseLong(id));

            	response.setStatus("00");
            	response.setData(null);
            	response.setMessage("Update was successful ");
            	
            	
            	 Staff staff = staffRepository.findUserByEmail(user_email);
                 
                 if(staff!=null) {
                 CreateActivityLog(staff.getName(),staff.getPhone(),staff.getEmail(),
 						 "Complete Support request");
                 }
            	
         
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("Account update failed"); // change this to - An error occur, please try again
        	
        	logger.info("Account update failed: "+ex.getMessage());
      
        }
        return response;
       
    }
    
    // first chat in admin portal Dasboard
    @GetMapping("/getlogginUserChartDetails")
    public Object getlogginUserChartDetails() {
    	
    	Response<Object> response = new Response<Object>();
    	
    	ChartModel model =new ChartModel();
        
    	try {
    		
    		String today= new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    		
    		 LocalDate last2wks = LocalDate.parse(today).minusWeeks(2);
    		 
    		 Date date2 = null,date1 = null;
    	     date1= new SimpleDateFormat("yyyy-MM-dd").parse(today);
    	     date2 = new SimpleDateFormat("yyyy-MM-dd").parse(last2wks.toString());
    	     List<Date> dateList = getDatesBetweenUsingJava7(date2,date1);
    	   
    	     String[] label = new String[dateList.size()];
    	     int[] data = new int[dateList.size()];
    	     
    	     for (int i=0; i<dateList.size(); i++){
    	     
    	    	 String dateVal = dateList.get(i).toString();
    	    	 String[] d = dateVal.split(" ");
    	    	 
    	    	 label[i] = ordinal(Integer.parseInt(d[2]));
    	    	 List<CustomerLoginRecord> list = customerLoginRecordRepository.findAllByDate_loggedin(dateList.get(i));
    	    	 
    	    	 if(list == null) {  
    	    		 
    	    		 data[i] = 0;
    	    		 
    	    	 }else {
    	    		 
    	    		 if(list.size()==0) {
    	    			
    	    			 data[i] = 0;
    	    			 
    	    		 }else {
    	    			 
    	    			 if(list.size()>200) {
    	    				 data[i] = 200;	 
    	    			 }else {
    	    				 data[i] = list.size();
    	    			 }
    	    		 }
    	    	 }
    	  
    	     }
    	     
    	     int[] part1 = new int[7];
    	     int[] part2 = new int[7];

    	     System.arraycopy(data, 0           , part1, 0     , part1.length);
    	     System.arraycopy(data, part1.length, part2, 0     , part2.length);
    	     
    	     
    	   model.setLabel(label);
    	   model.setThisWkdata(part1);
    	   model.setLastWkdata(part2);
    	     
             response.setStatus("00");
             response.setData(model);
             response.setMessage("Successfully");
    		
    	}catch(Exception ex) {
    		response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("failed to get configuration"); // change this to - An error occur, please try again
        	
        	logger.info("failed to get configuration: "+ex.getMessage());
    	}
    	
        return response;
    }
    
    // second chart in admin portal Dasboard
    @GetMapping("/getTransactionChartDetails")
    public Object getTransactionChartDetails() {
    	
    	Response<Object> response = new Response<Object>();
    	
    	ChartModel model =new ChartModel();
        
    	try {
    		
    		 List<String> list = getMonthList("MMM");
    		 
    		 List<String> datelist = getMonthList("MM-yyyy");
    		 
    		 String[]label =new String[list.size()];
    		 int[]data =new int[list.size()];
    		 
    		 for(int i=0;i<list.size(); i++) {
    			 label[i] = list.get(i);
    			 
    			 Date startDate= new SimpleDateFormat("dd/MM/yyyy").parse("01-"+datelist.get(i));
    			 Date endDate= new SimpleDateFormat("dd/MM/yyyy").parse("28-"+datelist.get(i));
    			 
    			 List<TransactionLog> translist = transactionLogRepository.findByTransdateBetween(startDate,endDate);
    			 
    			 if(translist==null) {
    				 data[i]=0;
    			 }else {
    				 data[i]=translist.size();
    			 }
    			 
    		 }
    		 
    		 model.setLabel(label);
    		 model.setThisWkdata(data);
 
             response.setStatus("00");
             response.setData(model);
             response.setMessage("Successfully");
    		
    	}catch(Exception ex) {
    		response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("failed to get configuration"); // change this to - An error occur, please try again
        	
        	logger.info("failed to get configuration: "+ex.getMessage());
    	}
    	
        return response;
    }
    
    

    @GetMapping("/getAppConfig")
    public Object getAppConfig() {
    	
    	Response<Object> response = new Response<Object>();
        
    	try {
    		
    		List<AppConfig> config =appConfigRepository.findAll();
    	     
             response.setStatus("00");
             response.setData(config.get(0));
             response.setMessage("Successfully");
    		
    	}catch(Exception ex) {
    		response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("failed to get configuration"); // change this to - An error occur, please try again
        	
        	logger.info("failed to get configuration: "+ex.getMessage());
    	}
    	
        return response;
    }
    
    

   // update app configuration
    
   @RequestMapping(value = "/updateAppConfig", method = RequestMethod.POST)
    public Object updateAppConfig(@RequestBody AppConfigModel req) {
    	
    	Response<Object> response = new Response<Object>();
        
    	try {
    		
    		appConfigRepository.updateAppConfig(req.getAppbase_url(),
    				req.getAppbase_url(),
    				req.getBank_acct_number(), req.getBank_code(), req.getCredit_acct_master(), 
    				req.getDebit_acct_master(), req.getDebit_acct_sub(),
    				req.getMinimum_investment(), req.getPaystack_callback_url(),Long.parseLong(req.getId()));
    	     
             response.setStatus("00");
             response.setData(null);
             response.setMessage("Account created Successfully");
             
             

        	 Staff staff = staffRepository.findUserByEmail(req.getLogging_user_email());
             
             if(staff!=null) {
             CreateActivityLog(staff.getName(),staff.getPhone(),staff.getEmail(),
						 "Complete Support request");
             }
             
    		
    	}catch(Exception ex) {
    		response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("failed to get configuration"); // change this to - An error occur, please try again
        	
        	logger.info("failed to get configuration: "+ex.getMessage());
    	}
    	
        return response;
        
    }
    
   
   /*
   private CustomerLoginRecord getCustomerDetail(String CustID) {
   	
	   CustomerLoginRecord val = customerLoginRecordRepository.getCustomerRecord(CustID);
   	
	   return val;
   }
 
   */
   
   
    
    
    private void updateActivityLog(String actionPerformed, String email) {
    	
    	// update Activity log with the action a user performed  
    	activityLogRepository.updateActivityLog(actionPerformed, email);
    	
    } 
    

    
    @Async
    private void CreateActivityLog(String loggedin_user,
			   							String phone,
			   							String email,
			   							String action_performed) {
    	
    	// When a User logged in, Activity log is created
    	
    	ActivityLog log =new ActivityLog();
    	log.setLoggedin_user(loggedin_user);
    	log.setPhone(phone);
    	log.setEmail(email);
    	log.setAction_performed(action_performed);
    	
    	
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        log.setLoggedin_date(formatter.format(date));
        
        activityLogRepository.save(log);
    } 
    
    public static String encryptPassword(String value) {
 	   String key = "JavasEncryptDemo"; // 128 bit key
        String randomVector = "RandomJavaVector"; // 16 bytes IV
 	
 	 try {
          IvParameterSpec iv = new IvParameterSpec(randomVector.getBytes("UTF-8"));
          SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
          Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
          cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
          byte[] encrypted = cipher.doFinal(value.getBytes());
         // System.out.println("encrypted text: "  + Base64.encodeBase64String(encrypted));
          return Base64.encodeBase64String(encrypted);
      } catch (Exception e) {
          e.printStackTrace();
      }
      return "UEpIrpddkzxrzS+Q3z/sWg==";

 }
 
    public static String decryptPassword(String encrypted) {
 	
 	   String key = "JavasEncryptDemo"; // 128 bit key
        String randomVector = "RandomJavaVector"; // 16 bytes IV
 	
 	  try {
           IvParameterSpec iv = new IvParameterSpec(randomVector.getBytes("UTF-8"));
           SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
           Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
           cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
           byte[] originalText = cipher.doFinal(Base64.decodeBase64(encrypted));
          // System.out.println("decrypted text: "  + new String(originalText));
           return new String(originalText);
       } catch (Exception e) {
           e.printStackTrace();
       }
       return "Password@111";
  
  
 }

 
  
    

    public static List<Date> getDatesBetweenUsingJava7( Date startDate, Date endDate) {
        List<Date> datesInRange = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);
        
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);
     
        while (calendar.before(endCalendar)) {
            Date result = calendar.getTime();
            datesInRange.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return datesInRange;
    }
    
    private String ordinal(int num)
    {
        String[] suffix = {"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
        int m = num % 100;
        return String.valueOf(num) + suffix[(m > 3 && m < 21) ? 0 : (m % 10)];
    }
    
    private List<String> getMonthList(String datePattern){
    	
    	List<String> list=new ArrayList();
     //   DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMM yyyy");
    	   DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern(datePattern);
        YearMonth thisMonth    = YearMonth.now();
        
        for(int i=0; i<7; i++){
        
        	list.add( thisMonth.minusMonths(i).format(monthYearFormatter));
    //   System.out.println( thisMonth.minusMonths(i).format(monthYearFormatter));
        
        }
        return list;
    }
    
    
}
