package com.coronationam.AdminPortal.modules.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.coronationam.AdminPortal.modules.entity.Redemption;


@Repository
public interface RedemptionRepository extends JpaRepository<Redemption, Long> {

 
    @Modifying
    @Query("update Redemption U set U.status = :status where U.id = :id")
    @Transactional
    int updateRedemptionReqeust(@Param("status") String status,
    		@Param("id") Long id);


	
	
	
}