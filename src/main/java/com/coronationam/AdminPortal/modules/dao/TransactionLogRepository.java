package com.coronationam.AdminPortal.modules.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.coronationam.AdminPortal.modules.entity.CustomerLoginRecord;
import com.coronationam.AdminPortal.modules.entity.TransactionLog;


@Repository
@Transactional
public interface TransactionLogRepository extends JpaRepository<TransactionLog, Long> {

    @Modifying
    @Query("update TransactionLog T set T.status = :status where T.transRef = :transRef")
    int updateTransLog(@Param("transRef") String transRef, @Param("status") String status);
    
   List<TransactionLog> findByTransdateBetween( Date datefrom,Date dateTo);
    
  //  @Query("select d from TransactionLog d where d.datetime <= :datetime")    
  //  List<TransactionLog> findAllByDatetimeBetween(@Param("datetime") Date datetime,@Param("datetime") Date datetime2);

   
   
}
