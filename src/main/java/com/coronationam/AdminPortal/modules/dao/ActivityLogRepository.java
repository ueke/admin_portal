package com.coronationam.AdminPortal.modules.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.coronationam.AdminPortal.modules.entity.ActivityLog;


@Repository
public interface ActivityLogRepository extends JpaRepository<ActivityLog, Long> {

	/*
	ActivityLog findUserByEmail(String email);

	
    @Modifying
    @Query("update ActivityLog U set U.email = :email where U.password = :password")
    int updatePassword(@Param("email") String email, @Param("password") String password);


    @Modifying
    @Query("update ActivityLog U set U.infoware_id = :custAid, U.acct_status=:status where U.userid = :tempId")
    int updateWithInfowareDetail(@Param("custAid") String custAid, @Param("tempId") String tempId, @Param("status") String status);
*/
	
	  
    @Modifying
    @Query("update ActivityLog U set U.action_performed = :action_performed where U.email = :email")
    @Transactional
    int updateActivityLog(@Param("action_performed") String action_performed,
    		@Param("email") String email);

	
}
