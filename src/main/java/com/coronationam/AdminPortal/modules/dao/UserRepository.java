package com.coronationam.AdminPortal.modules.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.coronationam.AdminPortal.modules.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByEmail(String email);

    @Modifying
    @Query("update User U set U.email = :email where U.password = :password")
    int updatePassword(@Param("email") String email, @Param("password") String password);


    @Modifying
    @Query("update User U set U.infoware_id = :custAid, U.acct_status=:status where U.userid = :tempId")
    int updateWithInfowareDetail(@Param("custAid") String custAid, @Param("tempId") String tempId, @Param("status") String status);

}
