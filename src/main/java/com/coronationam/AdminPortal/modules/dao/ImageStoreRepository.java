package com.coronationam.AdminPortal.modules.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.coronationam.AdminPortal.modules.entity.ImageStore;


@Repository
@Transactional
public interface ImageStoreRepository extends JpaRepository<ImageStore, Long> {

    @Query("SELECT t FROM ImageStore t where t.custid =:custId ")
    ImageStore getImage(@Param("custId") String custId);

    @Modifying
    @Query("update ImageStore T set T.path = :path where T.custid=:custId")
    int updateImage(@Param("custId") String custId, @Param("path") String path);


}
