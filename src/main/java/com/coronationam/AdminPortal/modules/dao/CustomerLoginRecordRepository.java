package com.coronationam.AdminPortal.modules.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.coronationam.AdminPortal.modules.entity.CustomerLoginRecord;




@Repository
public interface CustomerLoginRecordRepository extends JpaRepository<CustomerLoginRecord, Long> {



	List<CustomerLoginRecord> findByUserid(String custID, Pageable topTwo);
	
	 List<CustomerLoginRecord> findAllByDate_loggedin(Date date);

	

	
}
