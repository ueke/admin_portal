package com.coronationam.AdminPortal.modules.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.coronationam.AdminPortal.modules.entity.Roles;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Long> {
	
	
}
