package com.coronationam.AdminPortal.modules.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.coronationam.AdminPortal.modules.entity.AppConfig;


@Repository
public interface AppConfigRepository extends JpaRepository<AppConfig, Long> {



    @Modifying
    @Query("update AppConfig U set U.appbase_url = :appbase_url, U.infoware_baseurl=:infoware_baseurl, U.bank_acct_number = :bank_acct_number,"
    		+ " U.bank_code=:bank_code, U.credit_acct_master=:credit_acct_master, "
    		+ "U.debit_acct_master=:debit_acct_master,"
    		+ " U.debit_acct_sub=:debit_acct_sub ,"
    		+ " U.minimum_investment=:minimum_investment , U.paystack_callback_url=:paystack_callback_url where U.id = :id")
    @Transactional
    int updateAppConfig(@Param("appbase_url") String appbase_url, @Param("infoware_baseurl") String infoware_baseurl,
    		@Param("bank_acct_number") String bank_acct_number,@Param("bank_code") String bank_code,
    		@Param("credit_acct_master") String credit_acct_master,
    		@Param("debit_acct_master") String debit_acct_master,
    
    		@Param("debit_acct_sub") String debit_acct_sub,
    		@Param("minimum_investment") String minimum_investment,
    
    		@Param("paystack_callback_url") String paystack_callback_url,
    		@Param("id") Long id);

}