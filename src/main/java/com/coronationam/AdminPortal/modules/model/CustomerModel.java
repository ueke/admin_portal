package com.coronationam.AdminPortal.modules.model;

import java.io.Serializable;

public class CustomerModel implements Serializable{

	private String id;
	private String name;
	private String tempId;
	private String phone;
	private String email;
	private String date_created;
	private String date_loggedIn;
	
	private String dept;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDate_created() {
		return date_created;
	}

	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}

	public String getDate_loggedIn() {
		return date_loggedIn;
	}

	public void setDate_loggedIn(String date_loggedIn) {
		this.date_loggedIn = date_loggedIn;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}
	
	
	
	
	
}
