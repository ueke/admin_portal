package com.coronationam.AdminPortal.modules.model;

import java.io.Serializable;

public class StaffModel implements Serializable {

		private String id;
	  private String role;
	    private String email;
	    private String name;
	    
	  
		private String phone;
	    private String dept;
	    private String staffisactive;
	    
	    private String logging_user_email;
	    
	    
	    
		
		public String getLogging_user_email() {
			return logging_user_email;
		}
		public void setLogging_user_email(String logging_user_email) {
			this.logging_user_email = logging_user_email;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getDept() {
			return dept;
		}
		public void setDept(String dept) {
			this.dept = dept;
		}
		public String getStaffisactive() {
			return staffisactive;
		}
		public void setStaffisactive(String staffisactive) {
			this.staffisactive = staffisactive;
		}
	
		
		
		
	
		
	    
	    
	
}
