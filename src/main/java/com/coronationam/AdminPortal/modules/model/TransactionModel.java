package com.coronationam.AdminPortal.modules.model;

import java.io.Serializable;

public class TransactionModel implements Serializable {

private String id;
private String custid;
private String amt;
private String fundType;
private String channel;
private String Status;
private String date;

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}





public String getCustid() {
	return custid;
}
public void setCustid(String custid) {
	this.custid = custid;
}
public String getAmt() {
	return amt;
}
public void setAmt(String amt) {
	this.amt = amt;
}
public String getFundType() {
	return fundType;
}
public void setFundType(String fundType) {
	this.fundType = fundType;
}
public String getChannel() {
	return channel;
}
public void setChannel(String channel) {
	this.channel = channel;
}
public String getStatus() {
	return Status;
}
public void setStatus(String status) {
	Status = status;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}



	/*
	  
	 "":"1",
  "": "000940",
  "": "5000",
  "": "CMMFUND",
  "": "Paystack",
  "": "success",
  "": "6/8/2020 8:45:22"
	
	*/
	
}
