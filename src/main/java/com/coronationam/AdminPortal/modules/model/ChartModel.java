package com.coronationam.AdminPortal.modules.model;

import java.io.Serializable;

public class ChartModel implements Serializable{
	
	String[] label;
	int[] lastWkdata;
	int[] thisWkdata;
	
	
	public String[] getLabel() {
		return label;
	}
	public void setLabel(String[] label) {
		this.label = label;
	}
	public int[] getLastWkdata() {
		return lastWkdata;
	}
	public void setLastWkdata(int[] lastWkdata) {
		this.lastWkdata = lastWkdata;
	}
	public int[] getThisWkdata() {
		return thisWkdata;
	}
	public void setThisWkdata(int[] thisWkdata) {
		this.thisWkdata = thisWkdata;
	}

	

}
