package com.coronationam.AdminPortal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminPortalApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminPortalApiApplication.class, args);
	}

}
